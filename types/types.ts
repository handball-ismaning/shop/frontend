export interface Image {
  url: string
  caption: string
}
export interface Category {
  name: string
  slug: string
  inMenu: boolean
}
export interface Variant {
  id: string
  name: string
  inStock: number
  fullName: string
}
export interface Product {
  id: string
  images: Array<Image>
  name: string
  description: string
  inStock: number
  price: number
  categories: Array<Category>
  variants?: Array<Variant>
}
