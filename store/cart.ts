import { Variant } from '~/types/types'

interface CartItem {
  amount: number,
  product: Variant
}
interface State {
  currentItems: Array<CartItem>
}
export const state = () => ({
  currentItems: []
} as State)

export const mutations = {
  add(state: State, item: Variant) {
    let found = false
    state.currentItems = state.currentItems.map((current) => {
      if (current.product.id === item.id) {
        found = true
        return {
          ...current,
          amount: current.amount + 1
        }
      }
      return current
    })
    if (!found) {
      state.currentItems.push({
        amount: 1,
        product: item
      })
    }
  },
  remove(state: State, item: Variant) {
    state.currentItems = state.currentItems.filter((current) => {
      return current.product.id !== item.id
    })
  },
  updateCount(state: State, { item, count }: { item: Variant, count: number}) {
    state.currentItems = state.currentItems.map((current) => {
      if (current.product.id === item.id) {
        return {
          ...current,
          amount: count
        }
      }
      return current
    })
  }
}

export const getters = {
  countItems(state: State) {
    return state.currentItems.reduce((acc, current) => {
      return acc + current.amount
    }, 0)
  }
}
