import createPersistedState from 'vuex-persistedstate'
import { Context } from '@nuxt/types'

export default ({ store, isHMR }: Context) => {
  if (process.browser) {
    if (isHMR) {
      return
    }
    createPersistedState({
      storage: window.sessionStorage,
      key: 'isishop',
      paths: [
        'cart'
      ]
    })(store)
  }
}
